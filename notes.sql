-- Advance Selects in SQL

-- To exclude a certain condition
--  != is used to display all data except those in the condition
-- Example:
SELECT * FROM songs WHERE id != 1;
SELECT * FROM songs WHERE id != 5 AND album_id != 6;

-- To display a certain condition with comparison operators
-- Example:
SELECT * FROM songs WHERE length > 230;

-- To select multiple data from the same column
-- IN is used for multiple record
-- Example:
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("Pop", "Metal Rock");

-- To select data with similar characters
-- % is used for multiple characters
-- ab% is for data that starts with ab
SELECT * FROM songs WHERE song_name LIKE "th%";
-- %ab is for data that end with ab
SELECT * FROM songs WHERE song_name LIKE "%th";
-- %a% is for data that has an "a" character
SELECT * FROM songs WHERE song_name LIKE "%u%";
-- - is used for single characters
-- __t is for data that has exactly n number of blanks and then the characters
SELECT * FROM songs WHERE song_name LIKE "____t";

-- To display them by order
-- DESC for Descending
-- ASC for Ascending
SELECT * FROM songs ORDER BY length DESC;

-- To limit the number of data to be shown
SELECT * FROM songs LIMIT 5;

-- To show only one specific data only
SELECT DISTINCT genre FROM songs;

-- To join two tables
SELECT * FROM albums JOIN artists ON albums.id = artists.albums_id;
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;
SELECT name, album_title, date_released, song_name, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;
SELECT albums.name, albums.album_title, date_released, songs.song_name, songs.length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;
SELECT name AS band, album_title AS album, date_released, song_name AS song, songs.length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;
SELECT full_name, datetime_created, song_name, length, genre, album_title FROM users JOIN playlists ON users.id = playlists.user_id JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id JOIN albums ON songs.album_id = albums.id;

-- To add data into the tables
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("john", "john1234", "John Doe", 123456789, "John@mail.com", "New York");
INSERT INTO playlist (user_id, datetime_created) VALUES (1, "2022-12-22 09:53:00");
INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1,1), (1,10), (1,11);

-- https://joins.spathon.com/